﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class CubeSpawner : MonoBehaviour 
{
	public GameObject prefab;
	public InputField numberOfCubesInputField;
	public InputField radiusInputField;
	public Button regenarateButton;

	private int numberOfCubes = 7;
	private float radius = 3;
	private List<GameObject> cubeList = new List<GameObject> ();
	private List<GameObject> cubePool = new List<GameObject> ();

	void Awake()
	{
		InitInputFieldsFunctions ();
		InitializeInputFieldsDefaultValue ();
	}

	void Start()
	{
		SpawnInCircleRadius ();
	}

	void InitializeInputFieldsDefaultValue()
	{
		numberOfCubesInputField.text = numberOfCubes.ToString();
		radiusInputField.text = radius.ToString();
	}

	void InitInputFieldsFunctions()
	{
		numberOfCubesInputField.contentType = InputField.ContentType.IntegerNumber;
		radiusInputField.contentType = InputField.ContentType.DecimalNumber;

		numberOfCubesInputField.onValueChanged.AddListener (delegate(string arg0) {
			SetNumberOfCubes(arg0);
		});
		radiusInputField.onValueChanged.AddListener (delegate(string arg0) {
			SetRadius(arg0);
		});

		regenarateButton.onClick.AddListener (() => RegeneateCircle ());
	}

	void RegeneateCircle()
	{
		ClearAllCubes (delegate() {
			SpawnInCircleRadius();
		});
	}

	void SpawnInCircleRadius()
	{
		for (int i = 0; i < numberOfCubes; i++) 
		{
			GameObject _cube = GetNewOrPoolCube();
			float _degreePosition = (float)i / (float)numberOfCubes * 360f;
			_cube.transform.position = GetPositionInRadius (_degreePosition);

			cubeList.Add (_cube);
		}
	}

	void ClearAllCubes(Action _callback)
	{
		foreach (GameObject _cube in cubeList) {
			PoolObject (_cube);
		}
		cubeList.Clear();

		_callback ();
	}

	void SetNumberOfCubes(string _val)
	{
		numberOfCubes = int.Parse(_val);
	}

	void SetRadius(string _val)
	{
		radius = float.Parse(_val);
	}

	void PoolObject(GameObject _toPool)
	{
		_toPool.SetActive (false);
		cubePool.Add (_toPool);
	}

	Vector3 GetPositionInRadius(float _deg)
	{
		Vector3 _pos = transform.position;

		_pos.x += radius * Mathf.Sin (_deg * Mathf.Deg2Rad);
		_pos.y += radius * Mathf.Cos (_deg * Mathf.Deg2Rad);

		return _pos;
	}

	GameObject GetNewOrPoolCube()
	{
		GameObject _toReturnObj;
		if (cubePool.Count > 0) {
			_toReturnObj = cubePool [0];
			cubePool.RemoveAt (0);
			_toReturnObj.SetActive (true);
			return _toReturnObj;
		} else {
			_toReturnObj = Instantiate (prefab, transform) as GameObject;
			return _toReturnObj;
		}
	}
}